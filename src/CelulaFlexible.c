#include "CelulaFlexible.h"

#include <stdio.h>

void Crear_Maquina_CelulaFlexible(T_CONTEXTO_CELULA_FLEXIBLE * Contexto, double *Temperatura){
    Contexto->estado_anterior = E_CF_NULO;
    Contexto->estado_actual = E_CF_APAGADO;
    Contexto->estado_siguiente = E_CF_APAGADO;

    Contexto->Temperatura = Temperatura;
    Contexto->Referencia = 35;
    Contexto->TempHisteresis = 5;
    

    Crear_Maquina_Ventilador(& Contexto->contexto_Ventilador, Contexto->Temperatura, Contexto->Referencia, Contexto->TempHisteresis);
    Crear_Maquina_Ciclo(& Contexto->contexto_Ciclo);
    Crear_Maquina_Interfaz(&Contexto->contexto_Interfaz,Contexto->Referencia,Contexto->TempHisteresis);

    Contexto->Referencia = 35;
    Contexto->TempHisteresis = 5;
  
}

void Inicializar_Maquina_CelulaFlexible(T_CONTEXTO_CELULA_FLEXIBLE * Contexto){
    Contexto->estado_anterior = E_CF_NULO;
    Contexto->estado_actual = E_CF_APAGADO;
    Contexto->estado_siguiente = E_CF_APAGADO;

    Inicializar_Maquina_Ventilador(&(Contexto->contexto_Ventilador));
    (*(Contexto->contexto_Ciclo).Inicializar_Maquina)(&Contexto->contexto_Ciclo);
   
}

void Procesar_Evento_Maquina_CelulaFlexible(T_CONTEXTO_CELULA_FLEXIBLE * Contexto, T_EVENTO * evento){
    
    switch (Contexto->estado_actual){
    case E_CF_ENCENDIDO:
        if (Contexto->estado_anterior != E_CF_ENCENDIDO){
            Inicializar_Maquina_Ventilador(&Contexto->contexto_Ventilador);
            (*(Contexto->contexto_Ciclo).Inicializar_Maquina)(&Contexto->contexto_Ciclo);
            Inicializar_Maquina_Interfaz(&Contexto->contexto_Interfaz);
        }
        if ((Contexto->contexto_Ventilador).TempHisteresis != (Contexto->contexto_Interfaz).TempHisteresis1) {
            
            (Contexto->contexto_Ventilador).Referencia = (Contexto->contexto_Interfaz).Referencia1;
            (Contexto->contexto_Ventilador).TempHisteresis = (Contexto->contexto_Interfaz).TempHisteresis1;

        }
        else {
            Procesar_Evento_Maquina_Ventilador(&Contexto->contexto_Ventilador, evento);
            (*(Contexto->contexto_Ciclo).Procesar_Evento)(&Contexto->contexto_Ciclo, evento);
            Procesar_Evento_Maquina_Interfaz(&Contexto->contexto_Interfaz, evento);
        }
        switch (evento->evento_actual){
        case EV_EMERGENCIA:
            Contexto->estado_siguiente = E_CF_ERROR;
            
            break;
        case EV_POWER:
           
            Contexto->estado_siguiente = E_CF_APAGADO;
        default:
            break;
        }
        if (Contexto->estado_siguiente != Contexto->estado_actual)
        {
            Salir_Maquina_Ventilador(& Contexto->contexto_Ventilador);
            (*(Contexto->contexto_Ciclo).On_Exit)(& Contexto->contexto_Ciclo);
            Salir_Maquina_Interfaz(&Contexto->contexto_Interfaz);
        }
        break;
    case E_CF_ERROR:
        switch (evento->evento_actual){
        case EV_REARME:
            Contexto->estado_siguiente = E_CF_APAGADO;
            break;
        default:
            break;
        }
        break;
     case E_CF_APAGADO:
        switch (evento->evento_actual) {
        case EV_POWER:
            Contexto->estado_siguiente = E_CF_ENCENDIDO;
            
            break;
        default:
            break;

        }
    default:
        break;
    }
    Contexto->estado_anterior = Contexto->estado_actual;
    Contexto->estado_actual = Contexto->estado_siguiente;
}

void Imprimir_Estado_Maquina_CelulaFlexible(T_CONTEXTO_CELULA_FLEXIBLE * Contexto){
    printf("------------\r\n");
    

    switch (Contexto->estado_actual){
    case E_CF_APAGADO:
        printf("Celula Flexible apagada\r\n");
        break;
    case E_CF_ENCENDIDO:
        printf("Celula Flexible encendida\r\n");
        Imprimir_Estado_Maquina_Ventilador(& Contexto->contexto_Ventilador);
        (*(Contexto->contexto_Ciclo).Imprimir)(& Contexto->contexto_Ciclo);
        Imprimir_Estado_Maquina_Interfaz(&Contexto->contexto_Interfaz);
        break;
    case E_CF_ERROR:
        printf("Error; Pulse REARME \r\n");
    default:
        break;
    }
    printf("------------\r\n");
}
