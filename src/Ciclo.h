#ifndef CICLO_H
#define CICLO_H

#include "Eventos.h"

typedef enum Estados_Ciclo_Tag {E_CICLO_NULO = -1, E_CICLO_OP1, E_CICLO_OP2,
                                      E_CICLO_OP3, E_CICLO_OP4,
                                      E_CICLO_MAX_ESTADOS } T_ESTADOS_CICLO;

typedef struct Contexto_Ciclo_Tag T_CONTEXTO_CICLO;

typedef void (* TRANSICION_CICLO)(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento);

struct Contexto_Ciclo_Tag {
    T_ESTADOS_CICLO estado_actual;
    T_ESTADOS_CICLO estado_anterior;
    T_ESTADOS_CICLO estado_siguiente;
    int numero_estados;
    int numero_eventos;
    TRANSICION_CICLO const * Tabla_Transiciones;
    TRANSICION_CICLO Procesar_Evento;
    void (* On_Exit)(T_CONTEXTO_CICLO * Contexto);
    void (* Inicializar_Maquina)(T_CONTEXTO_CICLO * Contexto);
    void (* Imprimir)(T_CONTEXTO_CICLO * Contexto);

};

void Crear_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto);

#endif // CICLO_H
