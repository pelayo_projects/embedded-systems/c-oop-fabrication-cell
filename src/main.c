// Secuencia 1
#include "CelulaFlexible.h"
#include<stdio.h>
#define longitud_secuencia 23

static char * NombresEventos[longitud_secuencia] = {
    "EV_POWER",
    "EV_EMERGENCIA",
    "EV_REARME",
    "EV_POWER",
    "EV_FC1",
    "EV_FC2",
    "EV_TICK",
    "EV_TICK",
    "EV_TICK",
    "EV_FC3",
    "EV_FC4",
    "EV_OK",
    "EV_MENOS",
    "EV_OK",
    "EV_MAS",
    "EV_MAS",
    "EV_MAS",
    "EV_MAS",
    "EV_MAS",
    "EV_MAS",
    "EV_OK",
    "EV_TICK",
    "EV_POWER"

};




static ID_EVENTO SecuenciaEventos[longitud_secuencia] = { EV_POWER, EV_EMERGENCIA, EV_REARME, EV_POWER, EV_FC1, EV_FC2, EV_TICK, EV_TICK, EV_TICK, EV_FC3, EV_FC4, EV_OK, EV_MENOS, EV_OK, EV_MAS, EV_MAS, EV_MAS, EV_MAS, EV_MAS, EV_MAS, EV_OK, EV_TICK, EV_POWER };
static double SecuenciaTemperatura[longitud_secuencia] = { 37, 37, 37, 37, 37, 37, 37, 41, 29, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 41, 41 };



int TestManual(void)
{
    T_EVENTO e1;


    double Temperatura =37;

    T_CONTEXTO_CELULA_FLEXIBLE Contexto;

    Crear_Maquina_CelulaFlexible(& Contexto , & Temperatura);
    Inicializar_Maquina_CelulaFlexible(& Contexto);

    printf("ESTADO INICIAL\r\n");
    Imprimir_Estado_Maquina_CelulaFlexible(& Contexto);
    printf("Pulse Enter para continuar\r\n");
    getchar ();





    for (int i = 0; i < longitud_secuencia ; i++)
    {
        printf(" %s\r\n", NombresEventos[i]);

        if ((i > 0) && ( SecuenciaTemperatura[i] != SecuenciaTemperatura[i - 1]))
        {
            printf("Cambio de temperatura de %.1f a %.1f\n", SecuenciaTemperatura[i - 1],
                    SecuenciaTemperatura[i]);
            Temperatura = SecuenciaTemperatura[i];
        }


        Crear_Evento (&e1 , SecuenciaEventos[i]);
        Procesar_Evento_Maquina_CelulaFlexible(&Contexto , &e1);
        Imprimir_Estado_Maquina_CelulaFlexible(& Contexto);

        printf("***\n***\n***\n***\n");
        printf("Pulse Enter para continuar\r\n");


        getchar ();
    }


    return 0;
}

int main(void) {
    TestManual();
    printf("El test ha finalizado.");
    printf("Pulse Enter para finalizar\r\n");

    return getchar();

} //main
