#ifndef CELULAFLEXIBLE_H
#define CELULAFLEXIBLE_H


#include "Eventos.h"
#include "Ciclo.h"
#include "Ventilador.h"
#include "Interfaz.h"

typedef enum Estados_CelulaFlexible_Tag {E_CF_NULO = -1, E_CF_ENCENDIDO, E_CF_APAGADO, E_CF_ERROR, E_CF_MAX_ESTADOS } T_ESTADOS_CELULA_FLEXIBLE;

typedef struct Contexto_CelulaFlexible_Tag {
    T_ESTADOS_CELULA_FLEXIBLE estado_actual;
    T_ESTADOS_CELULA_FLEXIBLE estado_anterior;
    T_ESTADOS_CELULA_FLEXIBLE estado_siguiente;

    T_CONTEXTO_CICLO contexto_Ciclo;
    T_CONTEXTO_Ventilador contexto_Ventilador;
    T_CONTEXTO_Interfaz contexto_Interfaz;

    double *Temperatura;
    double Referencia;
    double TempHisteresis;

} T_CONTEXTO_CELULA_FLEXIBLE;

void Crear_Maquina_CelulaFlexible(T_CONTEXTO_CELULA_FLEXIBLE * Contexto, double *Temperatura);

void Inicializar_Maquina_CelulaFlexible(T_CONTEXTO_CELULA_FLEXIBLE * Contexto);

void Procesar_Evento_Maquina_CelulaFlexible(T_CONTEXTO_CELULA_FLEXIBLE * Contexto, T_EVENTO * evento);

void Imprimir_Estado_Maquina_CelulaFlexible(T_CONTEXTO_CELULA_FLEXIBLE * Contexto);


#endif // CELULAFLEXIBLE_H