#include"Eventos.h"
typedef enum Estados_Interfaz_Tag { E_INT_NULO = -1, E_INT_INICIAL, E_INT_REFERENCIA, E_INT_HISTERESIS, E_INT_MAX_ESTADOS } T_ESTADOS_Interfaz;

typedef struct Contexto_Interfaz_Tag {
    T_ESTADOS_Interfaz estado_actual;
    T_ESTADOS_Interfaz estado_anterior;
    T_ESTADOS_Interfaz estado_siguiente;

    double Referencia1;
    double TempHisteresis1;
} T_CONTEXTO_Interfaz;

void Crear_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto,double Referencia1, double TempHisteresis1);

void Inicializar_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto);

void Salir_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto);

void Procesar_Evento_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto, T_EVENTO* evento);

void Imprimir_Estado_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto);
