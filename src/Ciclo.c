#include "Ciclo.h"

#include <assert.h>
#include <stdio.h>



int operacion;
static char* NombresEstado[E_CICLO_MAX_ESTADOS] = {
    "Ciclo_OP1",
    "Ciclo_OP2",
    "Ciclo_OP3",
    "Ciclo_OP4"
};




static void Inicializar_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto);

static void Salir_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto);

static void Procesar_Evento_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento);

static void Imprimir_Estado_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto);

static void Ciclo_OP1(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento);

static void Ciclo_OP2(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento);

static void Ciclo_OP3(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento);

static void Ciclo_OP4(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento);


static TRANSICION_CICLO Tabla[E_CICLO_MAX_ESTADOS][EV_MAX_EVENTOS];



static void Ciclo_Vacio(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento){
    (void)Contexto;
    (void)evento;
}


static void Ciclo_OP1(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento){
   (void)evento;


    Contexto->estado_siguiente = E_CICLO_OP2;


}

static void Ciclo_OP2(T_CONTEXTO_CICLO *Contexto, T_EVENTO *evento){
   (void)evento;



    Contexto->estado_siguiente = E_CICLO_OP3;

}

static void Ciclo_OP3(T_CONTEXTO_CICLO *Contexto, T_EVENTO *evento){
    (void)evento;



    Contexto->estado_siguiente = E_CICLO_OP4;

}
static void Ciclo_OP4(T_CONTEXTO_CICLO *Contexto, T_EVENTO *evento){
    (void)evento;



    Contexto->estado_siguiente = E_CICLO_OP1;

}


void Crear_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto){

    Contexto->numero_estados = (int)E_CICLO_MAX_ESTADOS;
    Contexto->numero_eventos = (int)EV_MAX_EVENTOS;

    for(int i=0; i<Contexto->numero_estados; i++)
        for(int j=0; j<Contexto->numero_eventos; j++)
            Tabla[i][j]=Ciclo_Vacio;

    Tabla[E_CICLO_OP1][EV_FC1]=Ciclo_OP1;
    Tabla[E_CICLO_OP2][EV_FC2]=Ciclo_OP2;
    Tabla[E_CICLO_OP3][EV_FC3]=Ciclo_OP3;
    Tabla[E_CICLO_OP4][EV_FC4]=Ciclo_OP4;


    Contexto->Tabla_Transiciones = (TRANSICION_CICLO *) Tabla;

    Contexto->Procesar_Evento = Procesar_Evento_Maquina_Ciclo;
    Contexto->Inicializar_Maquina = Inicializar_Maquina_Ciclo;
    Contexto->On_Exit = Salir_Maquina_Ciclo;
    Contexto->Imprimir = Imprimir_Estado_Maquina_Ciclo;


    Contexto->estado_anterior = E_CICLO_NULO;
    Contexto->estado_actual = E_CICLO_OP1;
    Contexto->estado_siguiente = E_CICLO_OP1;

}

void Inicializar_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto){
    Contexto->estado_anterior = E_CICLO_NULO;
    Contexto->estado_actual = E_CICLO_OP1;
    Contexto->estado_siguiente = E_CICLO_OP1;
}

void Salir_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto){
    Contexto->estado_anterior = E_CICLO_NULO;
    Contexto->estado_actual = E_CICLO_OP1;
    Contexto->estado_siguiente = E_CICLO_OP1;
}

void Procesar_Evento_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto, T_EVENTO * evento){

    TRANSICION_CICLO transicion;
    int offset;

    assert(Contexto->estado_actual < Contexto->numero_estados);
    assert(Contexto->estado_actual > E_CICLO_NULO);
    assert(evento->evento_actual < Contexto->numero_eventos);

    offset = Contexto->estado_actual * Contexto->numero_eventos + evento->evento_actual;
    transicion = Contexto->Tabla_Transiciones[offset];
    (*transicion)(Contexto, evento);

    Contexto->estado_anterior = Contexto->estado_actual;
    Contexto->estado_actual = Contexto->estado_siguiente;

}

void Imprimir_Estado_Maquina_Ciclo(T_CONTEXTO_CICLO * Contexto){
    printf("------------\r\n");



    printf("Ciclo:  \r\n  %s \r\n",
         NombresEstado[Contexto->estado_actual]);


}

