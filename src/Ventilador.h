#ifndef VENTILADOR_H
#define VENTILADOR_H


#include "Eventos.h"

typedef enum Estados_Ventilador_Tag {E_VENT_NULO = -1, E_VENT_OFF, E_VENT_ON, E_VENT_MAX_ESTADOS } T_ESTADOS_Ventilador;

typedef struct Contexto_Ventilador_Tag {
    T_ESTADOS_Ventilador estado_actual;
    T_ESTADOS_Ventilador estado_anterior;
    T_ESTADOS_Ventilador estado_siguiente;

    double *Temperatura;
    double Referencia;
    double TempHisteresis;
} T_CONTEXTO_Ventilador;

void Crear_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto, double *Temperatura, double Referencia, double TempHisteresis);

void Inicializar_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto);

void Salir_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto);

void Procesar_Evento_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto, T_EVENTO * evento);

void Imprimir_Estado_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto);

#endif // VENTILADOR_H
