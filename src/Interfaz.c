#include"Interfaz.h"
#include<stdio.h>


static double referencia = 35;
static double temperaturahisteresis = 5;
static int evento1 = 0;
static int error_temp = 0;







void Crear_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto, double Referencia1, double TempHisteresis1) {

	Contexto->estado_anterior = E_INT_NULO;
	Contexto->estado_actual = E_INT_INICIAL;
	Contexto->estado_siguiente = E_INT_INICIAL;

	
	Contexto->Referencia1 = Referencia1;
	Contexto->TempHisteresis1 = TempHisteresis1;
	
	


};

void Inicializar_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto) {

	Contexto->estado_anterior = E_INT_NULO;
	Contexto->estado_actual = E_INT_INICIAL;
	Contexto->estado_siguiente = E_INT_INICIAL;
};

void Salir_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto) {

	Contexto->estado_anterior = E_INT_NULO;
	Contexto->estado_actual = E_INT_INICIAL;
	Contexto->estado_siguiente = E_INT_INICIAL;


};



void Procesar_Evento_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto, T_EVENTO* evento) {

	if (evento->evento_actual == EV_OK) { //SOLO RESPONDE A OK

		switch (Contexto->estado_actual) {

		case E_INT_INICIAL:

			Contexto->estado_siguiente = E_INT_REFERENCIA;




			break;



		case E_INT_REFERENCIA:
			evento1 = 0;




			Contexto->estado_siguiente = E_INT_HISTERESIS;

			break;
		case E_INT_HISTERESIS:
			evento1 = 0;
			
			Contexto->Referencia1 = referencia;

			Contexto->TempHisteresis1 = temperaturahisteresis;

			

			Contexto->estado_siguiente = E_INT_INICIAL;

			break;



		default:
			break;
		}
	}
	if (Contexto->estado_anterior != E_INT_NULO) {

		if (evento->evento_actual == EV_MAS) {
			error_temp = 0;
			switch (Contexto->estado_actual)
			{
			case E_INT_REFERENCIA:
				evento1 = 1;
				referencia = referencia + 1;
				break;
			case E_INT_HISTERESIS:
				evento1 = 3;
				
				if (temperaturahisteresis >= 1 && temperaturahisteresis < 10) {
					temperaturahisteresis = temperaturahisteresis + 1;
				}
				else {
					error_temp = 1;
				}
				break;

			default:
				break;
			}
		}

		if (evento->evento_actual == EV_MENOS) {
			error_temp = 0;
			switch (Contexto->estado_actual)
			{
			case E_INT_REFERENCIA:
				evento1 = 2;
				referencia = referencia - 1;
				break;
			case E_INT_HISTERESIS:
				evento1 = 4;
				
				if (temperaturahisteresis > 1 && temperaturahisteresis <= 10) {
					temperaturahisteresis = temperaturahisteresis - 1;
				}
				else {
					error_temp = 1;
				}
				break;

			default:
				break;
			}
		}
	}

	Contexto->estado_anterior = Contexto->estado_actual;
	Contexto->estado_actual = Contexto->estado_siguiente;

};
void Imprimir_Estado_Maquina_Interfaz(T_CONTEXTO_Interfaz* Contexto) {


	printf("------------\r\n");
	printf("Interfaz usuario: \r\n \t ");

	switch (Contexto->estado_actual) {
	case E_INT_INICIAL:
		printf("PULSE OK PARA CAMBIAR LA TEMPERATURA DE REFERENCIA \r\n");
		printf("PULSE (+) o (-) PARA CAMBIAR LOS VALORES\r\n");
		if (Contexto->estado_anterior != E_INT_INICIAL && Contexto->estado_anterior != E_INT_NULO) {
			printf("------------------");
			printf("Se han introducido los nuevos valores: \r\n Temperatura de referencia: %f \r\n Temperatura de Histeresis: %f \r\n"
				, Contexto->Referencia1, Contexto->TempHisteresis1);


			break;
	case E_INT_REFERENCIA:
		printf("PULSE OK PARA CAMBIAR LA TEMPERATURA DE HISTERESIS\r\n");
		if (evento1 == 1 ) {
			printf(" Temperatura de Referencia aumentada: % f �C\r\n", referencia);
		}
		if (evento1 == 2 ) {
			printf(" Temperatura de Referencia disminuida: % f �C\r\n", referencia);
		}
		

		break;
	case E_INT_HISTERESIS:
		printf("PULSE (+) o (-) PARA CAMBIAR LA TEMPERATURA DE HISTERESIS\r\n");

		if (evento1 == 3 && error_temp==0) {
			printf(" Temperatura de histeresis aumentada: % f �C\r\n", temperaturahisteresis);
		}
		if (evento1 == 4 && error_temp==0) {
			printf(" Temperatura de histeresis disminuida: % f �C\r\n", temperaturahisteresis);
		}
		if (error_temp == 1) {
			printf("*** LA TEMPERATURA DEBE ESTAR ENTRE 1 Y 10 ***\r\n");
			printf(" Temperatura de histeresis igual: % f �C\r\n", temperaturahisteresis);

		}

		break;
	default:
		break;
		}
		printf("------------\r\n");
	}

};