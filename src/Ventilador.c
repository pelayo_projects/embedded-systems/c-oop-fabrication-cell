#include"Ventilador.h"
#include<stdio.h>



static int Eventotick = 0;







void Crear_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto, double *Temperatura, double Referencia, double TempHisteresis){

    Contexto->estado_anterior = E_VENT_NULO; 
    Contexto->estado_actual = E_VENT_OFF; 
    Contexto->estado_siguiente = E_VENT_OFF;

    Contexto->Temperatura = Temperatura;
    Contexto->Referencia = Referencia;
    Contexto->TempHisteresis = TempHisteresis;




};

void Inicializar_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto){

    Contexto->estado_anterior = E_VENT_NULO;
    Contexto->estado_actual = E_VENT_OFF;
    Contexto->estado_siguiente = E_VENT_OFF;
};

void Salir_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto){     

    Contexto->estado_anterior=E_VENT_ON;
    Contexto->estado_actual=E_VENT_OFF;
    Contexto->estado_siguiente=E_VENT_OFF;


};

void Procesar_Evento_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto, T_EVENTO * evento){
    Eventotick = 0;
    if (evento->evento_actual == EV_TICK){ //SOLO RESPONDE A TICK
        Eventotick = 1;
        switch (Contexto->estado_actual){           

        case E_VENT_ON:                  


            if(*Contexto->Temperatura < (Contexto->Referencia - Contexto->TempHisteresis)){

                Contexto->estado_siguiente = E_VENT_OFF;
            };

            break;



        case E_VENT_OFF:


            if(*Contexto->Temperatura > (Contexto->Referencia + Contexto->TempHisteresis)){

                Contexto->estado_siguiente = E_VENT_ON;
            };
            break;

        default:
            break;
        }
        Contexto->estado_anterior = Contexto->estado_actual;
        Contexto->estado_actual = Contexto->estado_siguiente;
    }
};
void Imprimir_Estado_Maquina_Ventilador(T_CONTEXTO_Ventilador * Contexto){

    
    printf("------------\r\n");
    printf("Ventilador: \r\n \t ");

    switch (Contexto->estado_actual){
    case E_VENT_ON:
        printf("Ventilador encendido");
        if (Eventotick == 1) {
            printf(", Temperatura: % f ºC\r\n",*(Contexto->Temperatura));
        }
        break;
    case E_VENT_OFF:
        printf("Ventilador apagado");
        if (Eventotick == 1) {
            printf(", Temperatura: % f ºC\r\n", *(Contexto->Temperatura));
        }

        break;
    default:
        break;
    }
    printf("------------\r\n");

};