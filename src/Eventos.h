#ifndef EVENTOS_H
#define EVENTOS_H

typedef enum Id_Evento {EV_NULO=-1, EV_TICK, EV_POWER, EV_EMERGENCIA,
                       EV_REARME, EV_FC1, EV_FC2, EV_FC3, EV_FC4, EV_OK, EV_MAS, EV_MENOS, EV_MAX_EVENTOS} ID_EVENTO;
typedef struct Evento_Tag {
    ID_EVENTO evento_actual;
} T_EVENTO;

void Crear_Evento(T_EVENTO * evento, ID_EVENTO tipo);



#endif // EVENTOS_H
